<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class MemberNotLoginAuthentication 
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */
    public function handle($request, Closure $next)
    {
        if( !Auth::check() ){
            return $next($request);
        }else{
            $msg  = 'Bạn đã đăng nhập thành công';
            $style = "warning";
            return redirect()->route('trang-chu')->with(compact('msg','style'));
        }
    }
}
